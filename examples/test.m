format long
p=genpath('~/fvcopt');addpath(p);
%--------geometry------------%
shape = 'Cube';
Radius = 0.1;
% Discretization
nX = 20;

ObjProperties.Epsilon      = 10-1i*1;
ObjProperties.Mu           = 1;
ObjProperties.Temp_profile = 300;
ObjProperties.shape        = shape;
ObjProperties.Radius       = Radius;

% -------------------------------------------------------------------------
% OPTIONS - for the iterative solver and the SVD_tol
% -------------------------------------------------------------------------

OPTIONS.ITSOLVER    = 2; % ITSOLVER (1) for BICGSTAB, GMRES otherwise
OPTIONS.TOL         = 1e-4; % TOL tolerance for solver
OPTIONS.SVD_TOL     = 1e-4; % SVD_TOL tolerance for truncated SVD
OPTIONS.OUTER_IT    = 50; % OUTER_IT outer iterations for GMRES (for BICGSTAB the overall number is INNER_IT*OUTER_IT)
OPTIONS.INNER_IT    = 100; % INNER_IT inner iterations for GMRES
OPTIONS.VERBOSE     = 1; % VERBOSE (1) if wanna see print info, no printing as default
OPTIONS.PRECOND     = 2; % PRECOND: (0) no preconditioner
                         %          (1) left preconditioner for highly inhomogeneous objects
                         %          (2) left preconditioner for high contrast    

% -------------------------------------------------------------------------
% INPUT 
% -------------------------------------------------------------------------

[r,EMT] = getGeometry_1obj(nX,ObjProperties);

% Frequency range
freq = 299792458;
EM = em_var(freq);
[M] = getMopt(EMT.Er,EM.ce);
Gram = (r(2,1,1,1) - r(1,1,1,1))^3;
fprintf('\n N Operator');
[fN] = getOPERATORS(r,freq,'N');
fprintf('\n rSVD -  N Operator');
[U,S] = getUS(fN,M.idx,EM.ce,OPTIONS.SVD_TOL);
G0 = @(x)mv_G(x, fN, Gram, M.idx, EM.ce);

dof = ones(nX^3,1);
epsbkg=1;
epsdiff=-9;
epsimag=-0.1;

Power = opt_emissivity(dof,epsbkg,epsdiff,epsimag, EM, EMT, fN, G0,U, S, freq, OPTIONS, r);

B = (Radius*2)^2*6;
Power/B