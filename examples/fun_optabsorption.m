function fun_optabsorption(Radius,nX,bproj,eta,epsbkg,epsdiff,epsimag,tol,init,alg,Qabs)
% epsimag <0
p=genpath('~/fvcopt');addpath(p);
if isempty(gcp('nocreate'))
    parpool(5);
end
shape = 'sphere';

if Qabs > 0
    epsbkg = epsbkg * (1-1i/Qabs);
    epsimagn = imag((epsdiff+1i*epsimag) * (1-1i/Qabs));
    epsdiff = real((epsdiff+1i*epsimag) * (1-1i/Qabs));
    epsimag = epsimagn;
end

if strcmp(shape,'sphere')
    prefactor = 4/pi;
elseif strcmp(shape,'cube')
    prefactor = 1;
end
%--------geometry------------%
ObjProperties.Epsilon      = 10-1i*1;
ObjProperties.Mu           = 1;
ObjProperties.Temp_profile = 300;
ObjProperties.shape        = 'Cube';
ObjProperties.Radius       = Radius;
% -------------------------------------------------------------------------
% OPTIONS - for the iterative solver and the SVD_tol
% -------------------------------------------------------------------------

OPTIONS.ITSOLVER    = 2; % ITSOLVER (1) for BICGSTAB, GMRES otherwise
OPTIONS.TOL         = tol; % TOL tolerance for solver
OPTIONS.SVD_TOL     = 1e-4; % SVD_TOL tolerance for truncated SVD
OPTIONS.OUTER_IT    = 50; % OUTER_IT outer iterations for GMRES (for BICGSTAB the overall number is INNER_IT*OUTER_IT)
OPTIONS.INNER_IT    = 100; % INNER_IT inner iterations for GMRES
OPTIONS.VERBOSE     = 1; % VERBOSE (1) if wanna see print info, no printing as default
OPTIONS.PRECOND     = 0;
% -------------------------------------------------------------------------
% INPUT 
% -------------------------------------------------------------------------

[r,EMT] = getGeometry_1obj(nX,ObjProperties);
[select,BB] = shape_select(Radius,nX,r,shape);
if length(select>1)
    select = select(1:end/3);
end
% initial
n_=init;
if strcmp(init,'ones')
    dof = ones(nX^3,1);
elseif strcmp(init,'rand')
    dof = rand(nX^3,1);
elseif strcmp(init,'sphere')
    dof = zeros(nX,nX,nX);    
    object = @(r)( r(:,:,:,1).^2 + r(:,:,:,2).^2 + r(:,:,:,3).^2 < Radius^2);
    pointobject = object(r); % ones for domain elements in the sphere
    idx = find(pointobject(:));
    dof(idx)=1.0;
    dof=dof(:);
elseif strcmp(init,'vac')
    dof = zeros(nX^3,1);
    dof(randi(nX^3,1))=1;
else
    dof = load(init);
    dof = dof.dof;
    n_='';
end

% Frequency range
freq = 299792458;
EM = em_var(freq);
[M] = getMopt(EMT.Er,EM.ce);
hx = r(2,1,1,1) - r(1,1,1,1);
Gram = hx^3;
fprintf('\n N Operator');
[fN] = getOPERATORS(r,freq,'N');

% Define excitation - a plane wave
polarization = 'x';
k_vector = [0,0,EM.ko]; % direction
[Einc, Hinc] = planewave(r,k_vector,EM.omega_mu,polarization);
Pinc = 0.5*real(sum(sum(Einc(:,:,end,1).*conj(Hinc(:,:,end,2)))))*hx^2;
Pnormalize = Gram*prefactor/Pinc;

global ctrl F print name_;
ctrl=0;F=[];print=2;
name_=sprintf('absorption_%s_R%g_N%g_epsdiff%g_epsimag%g_b%g_e%g_alg%g_Q%g_',shape,Radius,nX,epsdiff,-epsimag,bproj,eta,alg,Qabs);
name_=strcat(name_,n_)
fun = @(x) opt_absorption(x,epsbkg,epsdiff,epsimag, EM, EMT, fN, Einc,OPTIONS, r, Pnormalize,select,1);

opt.max_objective = @(x) opt_filter(x,fun,bproj,eta);
opt.algorithm = alg;
opt.lower_bounds = zeros(nX^3,1);
opt.upper_bounds = ones(nX^3,1);
opt.ftol_rel=1e-12;
opt.verbose = 1;

[dof, fmax, retcode] = nlopt_optimize(opt, dof);

name2=strcat('./DATA/opt_',name_,sprintf('ctrl%d.mat',ctrl));
save(name2,'dof','epsdiff','epsbkg','epsimag');