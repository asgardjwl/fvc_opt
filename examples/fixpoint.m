function y = fixpoint(purcell,nX,ObjProperties,r,EMT,fN,U,S,M,Gram,EM,OPTIONS)

ObjProperties.Gain.purcell=abs(purcell);
[r,EMT] = getGeometry_1obj(nX,ObjProperties);
 
y=fvc_purcell(r, EMT, fN,U,S,M,Gram,EM,OPTIONS);
