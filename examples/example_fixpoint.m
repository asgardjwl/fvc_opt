clear;clc
p = genpath('~/etc/fvc');  % change it to the path of your fvc code
addpath(p);
format long

% this code is for omega=omega_21, you can easily add detunning by changing the file GetGeometry_1obj.m

% -------------------------------------------------------------------------
% Pumping paramters (all in SI unit)
% -------------------------------------------------------------------------
Gain.purcell=1; % initial guess, no Purcell enhancement (compare to vaccum)
% spectral
Gain.omega21=422e12*2*pi;
Gain.gperp=15.9e12*2*pi; % gamma_perpendicular
Gain.t21= 0.5e-9;Gain.t10=100e-15;Gain.t32=100e-15;
% atomic
Gain.eta=0.2; %quantum yield
Gain.Ntotal=24.46141568e24; %overall atomic population, the term "n" in Eq.(2)
%pumping
Gain.coherent=1;% "1" for coherent pump, incoherent otherwise
% P in Eq. (2) = gp*E2/max(E2), if the puming is nonuniform,
% otherwise P = gp
Gain.gp=6e9;
Gain.E2=1; % pumping field intensity distribution

freq=Gain.omega21/(2*pi);
ObjProperties.Gain        = Gain;
% objects without pumping
ObjProperties.Epsilon      = [4];
ObjProperties.Mu           = 1;
ObjProperties.Temp_profile = 300;  % will be overwritten with population inversion
ObjProperties.shape        = 'SphereGain';

% -------------------------------------------------------------------------
% OPTIONS - for the iterative solver and the SVD_tol (check other example files for details)
% -------------------------------------------------------------------------
OPTIONS.ITSOLVER    = 2;
OPTIONS.TOL         = 1e-4;
OPTIONS.OUTER_IT    = 50;
OPTIONS.INNER_IT    = 100;
OPTIONS.VERBOSE     = 0;
OPTIONS.PRECOND     = 2;
OPTIONS.SVD_TOL     = 1e-4;

% -------------------------------------------------------------------------
% initialize 
% -------------------------------------------------------------------------
% Discretization
nX = 40;
alpha = [1.0];
ObjProperties.Radius=alpha*299792458/Gain.omega21;

EM = em_var(freq);
[r,EMT] = getGeometry_1obj(nX,ObjProperties);
Gram = (r(2,1,1,1) - r(1,1,1,1))^3;
[M] = getM2D(EMT.Er,EMT.Tr,EM.ce,Gram,freq);
% Get the operator
fprintf('\n N Operator');
[fN] = getOPERATORS(r,freq,'N');

% Get truncated-SVD
fprintf('\n rSVD -  N Operator');
[U,S] = getUS(fN,M.idx,EM.ce,OPTIONS.SVD_TOL);

% -------------------------------------------------------------------------
% fixed point iteration 
% -------------------------------------------------------------------------
% x for current Purcell factor (a vector), returns the updated Purcell factor
g = @(x)fixpoint(x,nX,ObjProperties,r,EMT,fN,U,S,M,Gram,EM,OPTIONS);
% Anderson Accelaration parameters
mMax=10;
itmax=100;
atol=1e-10;
rtol=1e-5;
droptol=1e10;
beta=1;
AAstart=0;
% fixed point iteration
[purcell,iter,~] = AndAcc(g,Gain.purcell,mMax,itmax,atol,rtol,droptol,beta,AAstart);

%if you are interested in far-field emission pattern
% Nphi=1;Ntheta=20;
% dphi=pi/Nphi;dtheta=.5*pi/Ntheta;
% [phi,theta]=meshgrid(dphi/2:dphi:pi-dphi/2,dtheta/2:dtheta:pi/2-dtheta/2);
% sz=size(phi);
% phi=phi(:);theta=theta(:);

% DIRECTIONS.theta = theta;
% DIRECTIONS.phi   = phi;
% ObjProperties.Gain.purcell=purcell;
% [r,EMT] = getGeometry_1obj(nX,ObjProperties);
% U = fvc_directivity(DIRECTIONS,freq,r,EMT,OPTIONS,fN);
