ii=1;
% jj=5;

% etc.select = select;
% etc.EMT = EMT;
% etc.EM = EM;
% etc.nX = nX;
% etc.tau = ideal;
% etc.fN = fN;
% etc.fG0 = @(J) mv_G(J, fN, Gram, M.idx, 1.0);
% etc.Gram = Gram;
% etc.U = U1;
% etc.epsdiff = epsdiff;
% etc.epsimag = epsimag;
% etc.epsbkg = epsbkg;

% doftmp=dof;
% ind = 700;
% hx=1e-3;
% [y1,grad] = fun_ratio(doftmp,ii,etc);

% doftmp(ind) = doftmp(ind)+hx;
% [y2,grad] = fun_ratio(doftmp,ii,etc);
% (y2-y1)/hx,grad(ind)

ii_list = [1,2,3,4];
% jj_list = [1,2,3,4];
doftmp=dof(:);
ind = 700;
hx=1e-3;
[y1,grad] = fun_ratio_sum(doftmp,ii_list,etc,bproj,eta);

doftmp(ind) = doftmp(ind)+hx;
[y2,grad] = fun_ratio_sum(doftmp,ii_list,etc,bproj,eta);
(y2-y1)/hx,grad(ind)