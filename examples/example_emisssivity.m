% -------------------------------------------------------------------------
% EXAMPLE - GEOMETRY
% -------------------------------------------------------------------------
% shape = 'Sphere'
%       = 'Cube'
%       = 'Cylinder'

shape = 'Sphere';
nX = 40;
Radius = .02;
T=300;

ObjProperties.Epsilon      = -2-1i*1;
ObjProperties.Mu           = 1;
ObjProperties.Temp_profile = T;
ObjProperties.shape        = shape;
ObjProperties.Radius       = Radius;

% -------------------------------------------------------------------------
% OPTIONS - for the iterative solver and the SVD_tol
% -------------------------------------------------------------------------

OPTIONS.ITSOLVER    = 3; % ITSOLVER (1) for BICGSTAB, GMRES otherwise
OPTIONS.TOL         = 1e-4; % TOL tolerance for solver
OPTIONS.OUTER_IT    = 50; % OUTER_IT outer iterations for GMRES (for BICGSTAB the overall number is INNER_IT*OUTER_IT)
OPTIONS.INNER_IT    = 100; % INNER_IT inner iterations for GMRES
OPTIONS.VERBOSE     = 1; % VERBOSE (1) if wanna see print info, no printing as default
OPTIONS.PRECOND     = 0; % PRECOND: (0) no preconditioner
                         %          (1) left preconditioner for highly inhomogeneous objects
                         %          (2) left preconditioner for high contrast    
OPTIONS.SVD_TOL     = 1e-4; % SVD_TOL tolerance for truncated SVD


% -------------------------------------------------------------------------
% INPUT 
% -------------------------------------------------------------------------

% Discretization
[r,EMT] = getGeometry_1obj(nX,ObjProperties);

% Frequency range
freq = 299792458;

% -------------------------------------------------------------------------
% RUN fvc_emissivity
% -------------------------------------------------------------------------

[Power,Ranks] = fvc_emissivity(freq, r, EMT, OPTIONS);

C=4.79924335e-11; %C=h/k_b
h=6.62606957e-34;
pf=h*freq./(exp(C*freq./T)-1);

if strcmp(shape,'Cube')
    B = pf*(Radius*2)^2*6;
elseif strcmp(shape,'Sphere')
    B = pf*4*pi*Radius^2;
end

Power/pf
