#!/bin/bash
#SBATCH --job-name=5_02
#SBATCH --output=5_02.txt
#SBATCH --error=list.err
#SBATCH -N 1
#SBATCH --ntasks-per-node=12
#SBATCH --time=3000:00:00
#SBATCH --mem-per-cpu=4000
#SBATCH --partition=photon-newton

 np=12;
 shape="'sphere'"
 Radius=0.05;
 nX=40;
 epsbkg=1;
 epsdiff=-5;
 epsimag=-0.2
 svdtol=1e-4;
 tol=1e-4;
 init="'DATA/opt_emissivity_sphere_R0.05_N40_epsdiff-5_epsimag0.2_svd0.0001_b5_alg24ctrl_old124.mat'";
 bproj=10.0;
 alg=24;
  
 matlab -nodesktop -nosplash -nodisplay -r "opt_far($Radius,$nX,$bproj,$epsbkg,$epsdiff,$epsimag,$svdtol,$tol,$init,$alg,$np,$shape)"
