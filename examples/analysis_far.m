parpool;
p=genpath('~/fvcopt');addpath(p);
%file = 'DATA/opt_emissivity_sphere_R0.5_N40_epsdiff20_epsimag4_svd0.001_b40_alg24ctrl4.mat';
%file='DATA/opt_emissivity_sphere_R0.5_N40_epsdiff5_epsimag4_svd0.001_b40_alg24ctrl40.mat';
file='DATA/opt_emissivity_sphere_R0.05_N40_epsdiff-5_epsimag0.2_svd0.0001_b30_e1_alg24ctrl400.mat';
load(file)
shape = 'sphere';
Radius = 0.05;
nX = 40;
svdtol = 1e-3;
tol = 1e-4;

N_check=12;
%--------geometry------------%
ObjProperties.Epsilon      = 10-1i*1;
ObjProperties.Mu           = 1;
ObjProperties.Temp_profile = 300;
ObjProperties.shape        = 'Cube';
ObjProperties.Radius       = Radius;

OPTIONS.ITSOLVER    = 2;
OPTIONS.TOL         = tol;
OPTIONS.SVD_TOL     = svdtol;
OPTIONS.OUTER_IT    = 50;
OPTIONS.INNER_IT    = 100;
OPTIONS.VERBOSE     = 1;
OPTIONS.PRECOND     = 0;

[r,EMT] = getGeometry_1obj(nX,ObjProperties);
Gram = (r(2,1,1,1) - r(1,1,1,1))^3;
% Electromagnetic variables
freq = 299792458;
EM = em_var(freq);
% shape
[select,BB] = shape_select(Radius,nX,r,shape);
if select == 1
    select = ones(size(dof));
end
% Get matrices M
M = getM2D(EMT.Er,EMT.Tr,EM.ce,Gram, freq);
fprintf('\n N Operator');
[fN] = getOPERATORS(r,freq,'N');
fprintf('\n rSVD -  N Operator');
[U,S] = getUS_real(fN,M.idx,EM.ce,svdtol,select);

% dof
dof = dof.*reshape(select(1:end/3),size(dof));
EMT.Er = reshape(epsbkg + dof*(epsdiff+1i*epsimag),nX,nX,nX);
chiE  = EMT.Er(:) - 1.0;
chiE = [chiE;chiE;chiE];
[M] = getMopt(EMT.Er,EM.ce);

prefactor = 2*pi*freq*EM.eo/Gram;
S = prefactor * diag(S);

% ideal singular values
sigma = abs(epsdiff+1i*epsimag)^2/abs(epsimag);
ideal = ones(size(S))*sigma;
small = (0.5./S<ideal);
ideal(small)=0.5./S(small);

% preconditioner
% OPTIONS.PRECOND = 3;
% num = 6;
% OPTIONS.fun = @(x) 1e-2/max(ideal(1:num))*x+U(:,1:num)*(diag(1./ideal(1:num))*(U(:,1:num)'*x));
% actual singular values
tmp=[];
parfor ii =1:N_check
    tmp(ii,:) = solve_WU(U(:,ii).*chiE,fN,r,EMT,M,OPTIONS);
end
T = zeros(N_check,N_check);
for ii =1:N_check
    for jj = 1:N_check
        T(ii,jj) = sum(tmp(ii,:).*U(:,jj)')*Gram;
    end
end
T=T';

sum(abs(diag(T)).^2)/norm(T,'fro')^2
norm((T-T')/2,'fro')/norm(T,'fro')

% W
fA   = @(J)mv_AN(J, fN, EMT.Er, EMT.Er-1, Gram, 1, M.idx, 'notransp');
% parfor ii =1:N_check
%     tmp2 = fA(U(:,ii));
%     for jj = 1:N_check
%         W(ii,jj) = sum(tmp2.*conj(U(:,jj)));
%     end
% end

W = zeros(N_check,N_check);
for ii =1:N_check
    tmp2 = fA(U(:,ii));
    for jj = 1:N_check
        W(ii,jj) = Gram*sum(conj(U(:,jj)).*chiE.*U(:,ii))/sum(conj(U(:,jj).*tmp2));
    end
end

% select = real(chiE)>10;
% W = zeros(N_check,N_check);
% chiEtmp = chiE(select);
% parfor ii =1:N_check
%     tmp2 = fA(U(:,ii));
%     tmp2 = tmp2(select);
%     for jj = 1:N_check
%         tmpi = select.*U(:,ii);tmpi=tmpi(select);
%         tmpj = select.*U(:,jj);tmpj=tmpj(select);
%         W(ii,jj) = sum(conj(tmpj).*tmpi)/sum(conj(tmpj)./chiEtmp.*tmp2);
%     end
% end
%save('analysis_metal.mat')