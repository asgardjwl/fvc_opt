function [y,ys,ym,S] = bound_far(sigma,Radius,nX,svdtol,shape)
% BOUND_FAR - 
p=genpath('~/fvcopt');addpath(p);
if nargin <5
    shape = 'cube';
end
%--------geometry------------%
ObjProperties.Epsilon      = 10-1i*1;
ObjProperties.Mu           = 1;
ObjProperties.Temp_profile = 300;
ObjProperties.shape        = 'Cube';
ObjProperties.Radius       = Radius;

[r,EMT] = getGeometry_1obj(nX,ObjProperties);
Gram = (r(2,1,1,1) - r(1,1,1,1))^3;
% Electromagnetic variables
freq = 299792458;
EM = em_var(freq);
% shape
[select,BB] = shape_select(Radius,nX,r,shape);
% Get matrices M
M = getM2D(EMT.Er,EMT.Tr,EM.ce,Gram, freq);
fprintf('\n N Operator');
[fN] = getOPERATORS(r,freq,'N');
fprintf('\n rSVD -  N Operator');
[U,S] = getUS(fN,M.idx,EM.ce,svdtol,select);

prefactor = 2*pi*freq*EM.eo/Gram;
S=diag(S)*prefactor;

s1 = sum(S);
s2 = sum(S.^2);

for ii = 1:length(sigma)
    tmp = ones(size(S))*sigma(ii);
    small = (0.5./S<tmp);
    tmp(small)=0.5./S(small);

    y(ii) = 2/pi*sum(S.*tmp.*(1-S.*tmp))/BB;
    ym(ii) = 2/pi*sigma(ii)*sum(S)/BB;
    
    if sigma(ii)<=s1/2/s2
        ys(ii) = 2/pi*(s1*sigma(ii)-sigma(ii)^2*s2)/BB;
    else
        ys(ii) = 2/pi*s1^2/s2/4/BB;
    end
end