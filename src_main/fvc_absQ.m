function [P] = fvc_absQ(freq,r,EMT,OPTIONS,shape)

if nargin<5
    shape = 'sphere';
end
if strcmp(shape,'sphere')
    prefactor = 4/pi;
elseif strcmp(shape,'cube')
    prefactor = 1;
end
% voxel volume
hx = r(2,1,1,1) - r(1,1,1,1);
Gram = hx^3;

% Electromagnetic variables
EM = em_var(freq);

% Get the operators
[fN] = getOPERATORS(r,freq,'N');

% Get diagonal matrices with the material properties 
[M] = getM2D(EMT.Er,EMT.Tr,EM.ce,Gram,freq);

% -------------------------------------------------------------------------
% Solve the scattering problem
% -------------------------------------------------------------------------

% Define excitation - a plane wave
polarization = 'x';
k_vector = [0,0,EM.ko]; % direction
[Einc, Hinc] = planewave(r,k_vector,EM.omega_mu,polarization);
[xJ] = solve_WEinc(Einc,fN,r,EMT,M,freq,OPTIONS);

% [Esca] = mv_G(xJ, fN, Gram, M.idx, EM.ce);
% P = 0.5*real(sum(conj(xJ).*E))*Gram;

[GJ] = mv_symG(xJ, fN, M.idx, EM.ce);
P = 0.5*sum(conj(xJ).*(GJ+Einc(M.idx)))*Gram;

Pinc = 0.5*real(sum(sum(Einc(:,:,end,1).*conj(Hinc(:,:,end,2)))))*hx^2;
P = real(P)/Pinc*Gram*prefactor;
