function [val,grad] = fun_ratio(dof,ii,etc)
% etc ={invert, select, EMT, EM, nX, tau, fN, Gram, U, epsbkg, epsdiff, epsimag, fG0}
    
U = etc.U;
epsD = etc.epsdiff+1i*etc.epsimag;

dof = dof.*reshape(etc.select(1:end/3),size(dof));
etc.EMT.Er = reshape(etc.epsbkg + dof*(etc.epsdiff+1i*etc.epsimag),etc.nX,etc.nX,etc.nX);
chiE  = etc.EMT.Er(:) - 1.0;
chiE = [chiE;chiE;chiE];
[M] = getMopt(etc.EMT.Er,etc.EM.ce);

fW   = @(J) mv_AN(J, etc.fN, etc.EMT.Er, etc.EMT.Er-1, etc.Gram, 1, M.idx, 'notransp');

tmp1 = sum(U(:,ii).*fW(U(:,ii)))/etc.Gram;
tmp2 = sum(U(:,ii).*chiE.*U(:,ii));
if etc.invert == 0
    y = tmp2/tmp1+1i*etc.tau(ii);
else
    y = tmp1/tmp2+1/(1i*etc.tau(ii));
end

val = abs(y)^2;

if nargout>1
    grad_1 = U(:,ii).*etc.fG0(U(:,ii))/etc.Gram;
    grad_1 = -epsD*etc.select.*grad_1;
    grad_2 = U(:,ii).*U(:,ii).*etc.select*epsD;
    if etc.invert == 0
        grad = 2*real(conj(y)*(grad_2*tmp1-grad_1*tmp2)/tmp1^2);
    else
        grad = 2*real(conj(y)*(grad_1*tmp2-grad_2*tmp1)/tmp2^2);
    end
    grad=grad(1:end/3)+grad(1+end/3:end*2/3)+grad(1+end*2/3:end);
end