function [val,grad] = fun_WV_sum(dof,ii_list,jj_list,etc,bproj,eta,tif)
% OPT_FILTER - 
%   

if nargin<7
    tif = 0;
end

global ctrl print name_ F;
ctrl = ctrl + 1;
[dofnew,dofgrad] = bfilter_m(dof,bproj,eta);

N = length(ii_list);
val = 0.0;
grad = zeros(length(dof),1);

if nargout <2
    val = val + fun_WV(dofnew,ii_list(ii),jj_list(ii),etc);
else
    for ii = 1:N
        [y,gradtmp] = fun_WV(dofnew,ii_list(ii),jj_list(ii),etc);
        val = val + y*etc.contribute(ii_list(ii))^2;
        grad = grad + gradtmp(:).*dofgrad(:)*etc.contribute(ii_list(ii))^2;
        
        if tif == 1
            F = [F;[ctrl,ii_list(ii),jj_list(ii),y]];
            name1=strcat('./data_val_',name_,'.txt');
            save(name1,'-ascii','F')
        end
    end        
end

if mod(ctrl,print)==0 && tif == 1
    name2=strcat('./DATA/opt_',name_,sprintf('ctrl_%d.mat',ctrl));
    epsdiff = etc.epsdiff;
    epsbkg = etc.epsbkg;
    epsimag = etc.epsimag;
    save(name2,'dof','dofnew','epsdiff','epsbkg','epsimag');
end                

