function [val,grad] = fun_WV(dof,ii,jj,etc)
% etc ={select, EMT, EM, nX, tau, fN, Gram, U, epsbkg, epsdiff, epsimag, fG0}
    
U = etc.U;
epsD = etc.epsdiff+1i*etc.epsimag;

dof = dof.*reshape(etc.select(1:end/3),size(dof));
etc.EMT.Er = reshape(etc.epsbkg + dof*(etc.epsdiff+1i*etc.epsimag),etc.nX,etc.nX,etc.nX);
chiE  = etc.EMT.Er(:) - 1.0;
chiE = [chiE;chiE;chiE];
[M] = getMopt(etc.EMT.Er,etc.EM.ce);

fW   = @(J) mv_AN(J, etc.fN, etc.EMT.Er, etc.EMT.Er-1, etc.Gram, 1, M.idx, 'notransp');

tmp = fW(U(:,ii))/etc.Gram+chiE.*U(:,ii)/(1i*etc.tau(ii));
y = sum(U(:,jj).*tmp);

val = abs(y)^2;

if nargout>1
    g1 = U(:,jj).*U(:,ii).*etc.select*epsD/(1i*etc.tau(ii));
    g2 = U(:,jj).*etc.fG0(U(:,ii))/etc.Gram;
    g2 = -epsD*etc.select.*g2;
    grad = 2*real((g1+g2)*conj(y));
    grad=grad(1:end/3)+grad(1+end/3:end*2/3)+grad(1+end*2/3:end);
end