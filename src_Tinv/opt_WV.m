epsdiff = -5;
epsimag = -1;
epsbkg = 1;
Radius = 0.1;

Numi = 3;
Numj = 3;
diag_only = 0;
bproj = 0.0;
eta = 0.5;
alg = 24;
nX = 40;

dof = ones(nX^3,1);
%file = 'DATA/opt_Tinv_Ni3_Nj3_D0_sphere_R0.1_N40_epsdiff-5_epsimag0.5_svd0.001_b5_e0.5_alg24ctrl_82.mat';
%load(file)

p=genpath('~/fvcopt');addpath(p);
shape = 'sphere';
svdtol = 5e-3;
tol = 1e-4;
%--------geometry------------%
ObjProperties.Epsilon      = 10-1i*1;
ObjProperties.Mu           = 1;
ObjProperties.Temp_profile = 300;
ObjProperties.shape        = 'Cube';
ObjProperties.Radius       = Radius;

OPTIONS.ITSOLVER    = 2;
OPTIONS.TOL         = tol;
OPTIONS.SVD_TOL     = svdtol;
OPTIONS.OUTER_IT    = 50;
OPTIONS.INNER_IT    = 100;
OPTIONS.VERBOSE     = 1;
OPTIONS.PRECOND     = 0;

[r,EMT] = getGeometry_1obj(nX,ObjProperties);
Gram = (r(2,1,1,1) - r(1,1,1,1))^3;
% Electromagnetic variables
freq = 299792458;
EM = em_var(freq);
% shape
select = shape_select(Radius,nX,r,shape);
if select == 1
    select = ones(size(dof));
end
% Get matrices M
M = getM2D(EMT.Er,EMT.Tr,EM.ce,Gram, freq);
if ~exist('fN')
    fprintf('\n N Operator');
    [fN] = getOPERATORS(r,freq,'N');
end
if ~exist('U')
    fprintf('\n rSVD -  N Operator');
    [U,S] = getUS_real(fN,M.idx,EM.ce,svdtol,select);
end

% dof
dof = dof.*reshape(select(1:end/3),size(dof));
EMT.Er = reshape(epsbkg + dof*(epsdiff+1i*epsimag),nX,nX,nX);
chiE  = EMT.Er(:) - 1.0;
chiE = [chiE;chiE;chiE];
[M] = getMopt(EMT.Er,EM.ce);

if ~exist('ideal')
    prefactor = 2*pi*freq*EM.eo/Gram;
    Sp = prefactor * diag(S);

    % ideal singular values
    sigma = abs(epsdiff+1i*epsimag)^2/abs(epsimag);
    ideal = ones(size(Sp))*sigma;
    small = (0.5./Sp<ideal);
    ideal(small)=0.5./Sp(small)
    contribute = Sp.*ideal.*(1-Sp.*ideal)*4
    flux = 2/pi*sum(contribute)/(4*pi*Radius^2)/4
    flux_expect = sum(contribute(1:Numi))/sum(contribute)*flux
end

% setup opt parameters
etc.contribute = contribute;
etc.select = select;
etc.EMT = EMT;
etc.EM = EM;
etc.nX = nX;
etc.tau = ideal;
etc.fN = fN;
etc.fG0 = @(J) mv_G(J, fN, Gram, M.idx, 1.0);
etc.Gram = Gram;
etc.U = U;
etc.epsdiff = epsdiff;
etc.epsimag = epsimag;
etc.epsbkg = epsbkg;
global ctrl F print name_;
ctrl=0;F=[];T=[];print=2;
name_=sprintf('Tinv_Ni%g_Nj%g_D%g_%s_R%g_N%g_epsdiff%g_epsimag%g_svd%g_b%g_e%g_alg%g',Numi,Numj,diag_only,shape,Radius,nX,epsdiff,-epsimag,svdtol,bproj,eta,alg);

ii_list = 1:Numi;
jj_list = 1:Numj;
if diag_only == 0
    [ii_list,jj_list] = meshgrid(ii_list,jj_list);
end
opt.min_objective = @(x) fun_WV_sum(x,ii_list(:),jj_list(:),etc,bproj,eta,1);
opt.algorithm = alg;
opt.lower_bounds = zeros(nX^3,1);
opt.upper_bounds = ones(nX^3,1);
opt.ftol_rel=1e-12;
opt.verbose = 1;
[dof, fmax, retcode] = nlopt_optimize(opt, dof(:));