function [val,gradtotal] = fun_WV_minmax_constraint(dof_total,ii,jj,etc,bproj,eta,tif)
% OPT_FILTER - 
%   

if nargin<7
    tif = 1
end

dof = dof_total(2:end);
[dofnew,dofgrad] = bfilter(dof,bproj,eta);

if nargout <2
    val = fun_WV(dofnew,ii,jj,etc)-dof_total(1);
else
    [y,grad] = fun_WV(dofnew,ii,jj,etc);
    val = y - dof_total(1);
    
    for ii = 1:length(grad(:))
        grad(ii) = grad(ii) * dofgrad(ii);
    end
    gradtotal = [-1;grad];
    
    if tif == 1
        global ctrl print name_ F;
        
        F = [F;[ii,jj,y]];
        name1=strcat('./data_val_',name_,'.mat');
        save(name1,'F')
    
        if mod(ctrl,print)==0 && ii == 1 && jj == 1
            name2=strcat('./DATA/opt_',name_,sprintf('ctrl_%d.mat',ctrl));
            save(name2,'dof','dofnew');
        end                
    end

end
    
