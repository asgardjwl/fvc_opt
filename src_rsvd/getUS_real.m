function [U,S] = getUS_real(fN,idx,ce,eig_tol,select)
%% Computes the truncated (randomized) SVD of (symmetric) matrix
%% symG
if nargin <5
    select = 1;
end

% input for rSVD
Mrsvd = length(idx);
Nrsvd = length(idx);
Lmax = 1e-1;
tol  = eig_tol;
blocksize = 10;

% operator symG
Fdirect = @(x)mv_symG(x.*select, fN, idx, ce).*select;

% main function
[U,S] = rSVD_US_real(Fdirect,Fdirect,Mrsvd,Nrsvd,Lmax,tol,blocksize);

end
    