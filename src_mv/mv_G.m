function [JOut] = mv_G(JIn, fN, Gram, idx, ce)
%% Function that applies the operator symG: (G + ctranspose(G))/2

% the non-transpose component
JOut = mv_AN(JIn, fN, -1, -1, Gram , ce, idx, 'notransp');
end
