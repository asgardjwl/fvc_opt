function [select,BB] = shape_select(Radius,nX,r,shape)
% SHAPE_SELECT - 

if strcmp(shape,'cube')
    select = 1;
    BB = (Radius*2)^2*6;
elseif strcmp(shape,'sphere')
    BB = 4*pi*Radius^2;
    select = zeros(nX,nX,nX);    
    object = @(r)( r(:,:,:,1).^2 + r(:,:,:,2).^2 + r(:,:,:,3).^2 < Radius^2);
    pointobject = object(r); % ones for domain elements in the sphere
    idx = find(pointobject(:));
    select(idx)=1.0;
    select=select(:);
    select=[select;select;select];
end
