% gradient of J E = J G0 W J
% G = i omega e0 (J Gt) (Gt J)
G0 = @(x)mv_G(x, fN, Gram, M.idx, EM.ce);
[Mx,My,Mz,~] = size(r);
J=rand(length(M.idx),1);
del=1e-4;

%1dof
EMT.Er = reshape(epsbkg + dof*(epsdiff-1i*epsimag),Mx,My,Mz);
[M] = getMopt(EMT.Er,EM.ce);

E = solve_WU(J,fN,r,EMT,M,OPTIONS);
E = G0(E);
val = J.'*E;

G=1i*2*pi*freq*EM.eo*E.^2*(epsdiff-1i*epsimag);
G=G(1:end/3)+G(1+end/3:end*2/3)+G(1+end*2/3:end);
G(1)

%1dof
dof(1)=dof(1)+del;
EMT.Er = reshape(epsbkg + dof*(epsdiff-1i*epsimag),Mx,My,Mz);
[M] = getMopt(EMT.Er,EM.ce);

E = solve_WU(J,fN,r,EMT,M,OPTIONS);
E = G0(E);

val2 = J.'*E;
(val2-val)/del