function [Power,Grad] = opt_scattering(dof, epsbkg, epsdiff, epsimag, EM, EMT, fN, Einc,OPTIONS, r, Pnormalize,select,tif)
%% Computes the overall thermal radiating energy to the far-field
    
if nargin<13
    tif=1;
end
% voxel volume
hx = r(2,1,1,1) - r(1,1,1,1);
Gram = hx^3;
[Mx,My,Mz,~] = size(r);
% dof to Er
if select == 1
    select = ones(size(dof));
end
dof = dof.*reshape(select,size(dof));
EMT.Er = reshape(epsbkg + dof*(epsdiff+1i*epsimag),Mx,My,Mz);
% Get diagonal matrices with the material properties 
[M] = getMopt(EMT.Er,EM.ce);

% compute objective
[xJ] = solve_WEinc2(Einc,fN,r,EMT,M,EM,OPTIONS);
[GJ] = mv_symG(xJ, fN, M.idx, EM.ce);
P = -0.5*sum(conj(xJ).*GJ)*Gram;
Power = real(P)*Pnormalize;

if nargout>1
    % X = conj(W^\dagger GJ)
    [X] = solve_WtransU(GJ,fN,r,EMT,M,OPTIONS);
    
    % Etot
    [Esca] = mv_G(xJ, fN, Gram, M.idx, EM.ce);
    Etot = Esca + Einc(:);
    G = imag(EM.omega*EM.eo*(epsdiff+1i*epsimag)*conj(X).*Etot*Gram);
    Grad=G(1:end/3)+G(1+end/3:end*2/3)+G(1+end*2/3:end);
    Grad=select.*Grad*Pnormalize;
end
if tif
    global ctrl F print name_;
    ctrl=ctrl+1;
    F=[F Power];    
    
    name1=strcat('./data_',name_,'.mat');
    save(name1,'ctrl','F')
    if mod(ctrl,print)==0
        name2=strcat('./DATA/opt_',name_,sprintf('ctrl%d.mat',ctrl));
        save(name2,'dof','epsdiff','epsbkg','epsimag');
    end            
end
end