function [dofnew,dofgrad] = bfilter(dof,bproj,eta)
% BFILTER - 

dofnew = zeros(size(dof));
gradnew = zeros(size(dof));

for ii = 1:length(dof(:))
    if dof(ii) <= eta
        dofnew(ii) = eta * ( exp(-bproj*(1-dof(ii)/eta)) - (1-dof(ii)/eta)*exp(-bproj) );
        dofgrad(ii) = eta * ( (bproj/eta)*exp(-bproj*(1-dof(ii)/eta)) + exp(-bproj)/eta );
    else
        dofnew(ii) = (1-eta) * ( 1 - exp(-bproj*(dof(ii)-eta)/(1-eta)) + (dof(ii) - eta)/(1-eta) * exp(-bproj)) + eta;
        dofgrad(ii) = (1-eta) * ( bproj/(1-eta) * exp(-bproj*(dof(ii)-eta)/(1-eta )) + exp(-bproj)/(1-eta) );
    end
end
