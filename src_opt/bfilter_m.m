function [dofnew,dofgrad] = bfilter_m(dof,bproj,eta)
% BFILTER - 

dofnew = zeros(size(dof));
gradnew = zeros(size(dof));


more = (dof>eta);
dofnew = eta * ( exp(-bproj*(1-dof/eta)) - (1-dof/eta)*exp(-bproj) );
dofgrad = eta * ( (bproj/eta)*exp(-bproj*(1-dof/eta)) + exp(-bproj)/eta );


dofnew(more) = (1-eta)*(1-exp(-bproj*(dof(more)-eta)/(1-eta))+(dof(more)-eta)/(1-eta)*exp(-bproj)) + eta;
dofgrad(more) = (1-eta)*( bproj/(1-eta)*exp(-bproj*(dof(more)-eta)/(1-eta))+exp(-bproj)/(1-eta));
end
