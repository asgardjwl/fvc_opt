function [M] = getMopt(er,ce)
%% Get diagonal matrices with the material properties.

d_chiEx  = er(:) - 1.0;
chiE = [d_chiEx;d_chiEx;d_chiEx];

M.idx = 1:length(chiE(:));
Ndof_obj = length(M.idx);

M.MchiE    = spdiags( chiE, 0, Ndof_obj, Ndof_obj );
M.Id = speye(Ndof_obj,Ndof_obj);
M.Mer = M.MchiE + M.Id;
M.dof = Ndof_obj;
