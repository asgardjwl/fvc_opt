function [y,grad] = f_minimax(dof_total,tif)
% F_MINIMAX - 
%
if nargin<2
    tif=1;
end

y=dof_total(1);

if nargout>1
    grad = zeros(size(dof_total));
    grad(1) = 1;
end


if tif
    global ctrl T name_;
    ctrl=ctrl+1;
    T=[T y];    
    
    name1=strcat('./data_dT_',name_,'.mat');
    save(name1,'ctrl','T')
end