function [Power,Grad] = opt_emissivity(dof, epsbkg, epsdiff, epsimag, EM, EMT, fN, G0, U, S, freq, OPTIONS, r, normalize,select,tif)
%% Computes the overall thermal radiating energy to the far-field
    
if nargin<16
    tif=1;
end
% voxel volume
Gram = (r(2,1,1,1) - r(1,1,1,1))^3;
[Mx,My,Mz,~] = size(r);
omega = 2*pi*freq;
% dof to Er
if select == 1
    select = ones(size(dof));
end
dof = dof.*reshape(select,size(dof));
EMT.Er = reshape(epsbkg + dof*(epsdiff+1i*epsimag),Mx,My,Mz);
% Get diagonal matrices with the material properties 
[M] = getMopt(EMT.Er,EM.ce);
% -------------------------------------------------------------------------
% Compute
%         XU = W' * U
% where W = inv(A)
% -------------------------------------------------------------------------
XU = zeros(size(U));

% Solve the system for each singular vector
tic
parfor nU = 1:size(U,2)
    [XU(:,nU)] = solve_WtransU(U(:,nU),fN,r,EMT,M,OPTIONS);
end
Time_XU = toc;
fprintf('Time_XU      = %dm%ds  \n\n' ,floor(Time_XU/60),int64(mod(Time_XU,60)))

% Square roots of matrices D & S
L_S = sqrtm(S);

L_A = sqrt(-1.0*Gram*EM.eo*imag(diag( M.MchiE )));
L_A = diag(L_A);

% Final results
Power = 2/pi*omega*norm( (L_A * XU * L_S),'fro')^2;
Power = Power/normalize;
if nargout>1
    % G1 = -2/pi*omega*e0*imchx*(XU*S*XU^\dagger)_alpha
    G1 = -epsimag*Gram*2/pi*omega*EM.eo*sum(abs(XU*L_S).^2,2);
    
    % G2 = 2/pi*i*omega*e0*chi*(G0(WAA XU) S XU^\dagger)_alpha
    X = zeros(size(U));
    tic
    parfor nU = 1:size(U,2)
           [X(:,nU)] = solve_WU(XU(:,nU).*imag(diag(M.MchiE))*EM.eo*Gram*-1,fN,r,EMT,M,OPTIONS);
        X(:,nU) = G0(X(:,nU));
    end
    Time_XU = toc;
    fprintf('Time_XU      = %dm%ds  \n\n' ,floor(Time_XU/60),int64(mod(Time_XU,60)))

    G2 = 2/pi*1i*omega^2*EM.eo*(epsdiff+1i*epsimag)*sum(X*S.*conj(XU),2);
    G2 = 2*real(G2);
    
    G=G1+G2;
    Grad=G(1:end/3)+G(1+end/3:end*2/3)+G(1+end*2/3:end);
    Grad=select.*Grad/normalize;
end
if tif
    global ctrl F print name_;
    ctrl=ctrl+1;
    F=[F Power];    
    
    name1=strcat('./data_',name_,'.mat');
    save(name1,'ctrl','F')
    if mod(ctrl,print)==0
        name2=strcat('./DATA/opt_',name_,sprintf('ctrl%d.mat',ctrl));
        save(name2,'dof','epsdiff','epsbkg','epsimag');
    end            
end
end