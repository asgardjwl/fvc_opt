function [val,grad] = opt_filter(dof,fun,bproj,eta)
% OPT_FILTER - 
%   

[dofnew,dofgrad] = bfilter(dof,bproj,eta);

if nargout <2
    val = fun(dofnew);
else
    global ctrl print name_;
    [val,grad]=fun(dofnew);
    
    if mod(ctrl,print)==0
        name2=strcat('./DATA/opt_',name_,sprintf('ctrl_old%d.mat',ctrl));
        save(name2,'dof');
    end                
    
    for ii = 1:length(grad(:))
        grad(ii) = grad(ii) * dofgrad(ii);
    end
end
    
