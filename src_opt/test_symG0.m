[U,S] = getUS(fN,M.idx,EM.ce,OPTIONS.SVD_TOL);
symG0 = @(x)mv_symG(x, fN, M.idx, EM.ce);

x=rand(length(M.idx),1);
y1=-1*symG0(x);
y2=U*S*U'*x;

norm(y1-y2)/norm(y1)